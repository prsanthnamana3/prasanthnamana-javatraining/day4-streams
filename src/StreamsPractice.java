import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StreamsPractice {
    public static void main(String[] args){
        Stream<String> stream = Stream.of("a","b","c","d");
        String[] arr = {"a","b","c","d"};
        Stream<String> arrStream = Stream.of(arr);
        List<String>  list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        Stream<String> listStream = list.stream();
        arrStream.forEach(i-> System.out.println(i));
        arrStream.forEach(i-> System.out.println(i));
        Stream.concat(arrStream,stream).forEach(i-> System.out.println(i));




    }
}
